﻿using System.Reflection;

namespace CadastroAlunos
{
    internal class Program {
        static void Main(string[] args){
            Console.WriteLine("Cadastro Alunos");
            Aluno aluno1 = new("23423", "Joao", 18);
            Aluno aluno2 = new("12312", "Maria", 18);
            Aluno aluno3 = new("78437", "Pedro", 19);
            Aluno aluno4 = new("83734", "Joana", 19);

            aluno1.AdicionarNota(100.00);
            aluno2.AdicionarNota(80.00);
            aluno3.AdicionarNota(70.00);
            aluno4.AdicionarNota(80.00);

            Turma turma = new("3-A");
            turma.CadastrarAlunos(aluno1);
            turma.CadastrarAlunos(aluno2);
            turma.CadastrarAlunos(aluno3);
            turma.CadastrarAlunos(aluno4);

            turma.CalcularMedia();
            turma.mostrarMedia();
        }
    }
}