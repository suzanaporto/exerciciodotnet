using System.Reflection;

class Aluno {
    public string Matricula { get; set; }
    public string Nome { get; set; }
    public int Idade { get; set; }
    public double Nota { get; set; } = 0.0;

    public Aluno(string matricula, string nome, int idade) {
        Matricula = matricula;
        Nome = nome;
        Idade = idade;
    }

    public void AdicionarNota(double nota){
        Nota = nota;
    }
}